/**
 * SCAD design for the BOTTOM part of a case for the Raspberry Pi 1 Model B 
 * All measurements are in MM.
 */

/**
 * Since I don't care about the cylindrical ports right now this will be used to make a block
 * on the left to just make space. Everything is done relative to the width so if you want to
 * set this to 0 everything will still be in the right orientation, but you will need to
 * cut holes for the cylindrical ports.
 */
pcb_thickness=1.5;
pi_resting_height=5.5;
cyl_port_spacer=8.5;
//length=85; // original length (fits tight)
length=87;
width=56 + cyl_port_spacer;
base_height_max=pi_resting_height + 4.5;
wall_border_width=1.75;
eps=0.5; // fudge factor
half_eps=eps / 2.0; // half of that fudge

// Cylindrical place holders
cp1_offset_y=width - 43.6;
cp1_offset_x=length - 80.5;
cp2_offset_y=width - 17.75;
cp2_offset_x=length - 25.9;
cp_base_radius=2.8;
cp_base_height=pi_resting_height;
cp_through_radius = 1.4;
cp_holder_height=6;
cp_through_height=pcb_thickness + cp_holder_height;


// USB Dimensions
usb_width=13.5;
usb_height=15.3;
usb_y_offset=width - 35.75;

// Ethernet Dimensions
usb_to_eth=6.25;
eth_width=16;
eth_height=13.7;

// HDMI Dimension
hdmi_plug_width=18; // width of HDMI plug along X
hdmi_plug_height=5; // Z 
hdmi_offset_x=length - 53.75; // how far the HDMI plug is from the bottom

// Power Dimensions
power_width=9.75;
power_height=3;
power_y_offset=width - 1 - power_width;

// SD Card Dimensions
sdcard_width = 25; // note giving it 0.5 mm on each side

module translate_cube(x, y, z, cx, cy, cz) {
	translate([x,y,z]) {
		cube([cx, cy, cz]);
	}
}

module general_shape(height) {
	difference() {
		// base block
		cube([
				length + 2*wall_border_width,
				width + 2*wall_border_width,
				height + wall_border_width,
			]);
		// cut out inside
		translate([
			wall_border_width,
			wall_border_width,
			wall_border_width,
		]) {
			cube([
				length,
				width,
				height + half_eps,
			]);
		}
	}
}

module corner_base(tx, x, y) {
	translate([tx, -wall_border_width, -wall_border_width]) {
		base_cyl_holder(
			x, y, base_z=base_height_max + wall_border_width,
			through_z=top_height_max/2
		);
	}
}

corner_base(-wall_border_width, 0, 0);
corner_base(-wall_border_width, 0, width + 2*wall_border_width);
corner_base(wall_border_width, length, 0);
corner_base(wall_border_width, length, width + 2*wall_border_width);

// cuts out a piece of a wall parallel to the y axis
module remove_y_wall(y, z, cy, cz, x=0) {
	translate_cube(
		x-half_eps, y, z,
		wall_border_width + eps, cy, cz + eps
	);
}

// cuts out a piece of a wall parallel to the x axis
module remove_x_wall(x, z, cx, cz, y=0) {
	translate_cube(
		x, y - half_eps, z,
		cx, wall_border_width + eps, cz + eps
	);
}

module usb(z_offset, height=base_height_max) {
	remove_y_wall(
		usb_y_offset - half_eps,
		z_offset,
		usb_width + eps,
		height
	);
}

module ethernet(z_offset, height=base_height_max) {
	remove_y_wall(
		usb_y_offset + usb_width + usb_to_eth - half_eps,
		z_offset,
		eth_width + eps,
		height
	);
}

module hdmi(z_offset, height=base_height_max) {
	remove_x_wall(
			wall_border_width + hdmi_offset_x - half_eps,
			z_offset,
			hdmi_plug_width + eps,
			height,
			y=wall_border_width + width
	);
}

module power(z_offset, height=base_height_max) {
	remove_y_wall(
		power_y_offset - half_eps,
		z_offset,
		power_width + eps,
		height,
		x=wall_border_width + length
	);
}

base_z_offset=pi_resting_height + wall_border_width;

// build the base
difference() {
	general_shape(base_height_max);
	usb(base_z_offset);
	ethernet(base_z_offset);
	hdmi(base_z_offset);
	power(base_z_offset);
	translate_cube(
		length + wall_border_width - half_eps,	
		power_y_offset - 4.5 - sdcard_width,
		2*wall_border_width,
		wall_border_width + eps,
		sdcard_width,
		base_height_max
	);
}

module base_cyl_holder(x, y, base_z=cp_base_height, through_z=cp_through_height) {
	translate([
		x + wall_border_width,
		y + wall_border_width,	
		wall_border_width,
	]) {
		cylinder(h=base_z, r=cp_base_radius);
		translate([0, 0, base_z]) {
			cylinder(h=through_z, r=cp_through_radius);
		}
	}
}

base_cyl_holder(cp1_offset_x, cp1_offset_y);
base_cyl_holder(cp2_offset_x, cp2_offset_y);

// build the top
module top_cyl_holder(x, y) {
	height=top_height_max - pcb_thickness;
	translate([
		x + wall_border_width,
		y + wall_border_width,	
		wall_border_width,
	]) {
		difference() {
			cylinder(h=height, r=cp_base_radius);
			translate([0, 0, -half_eps]) {
			cylinder(h=height + eps, r=cp_through_radius + eps/2.75);
			}
		}
	}
}

module corner_top(x, y, oh, ih) {
	translate([x, y, 0]) {
		difference() {
			cylinder(h=wall_border_width + top_height_max, r=cp_base_radius);
			translate([0, 0, wall_border_width + half_eps]) {
				cylinder(h=top_height_max + eps, r=cp_through_radius + eps/2.75);
			}
		}
	}
}

module corner_top_space(x, y) {
	translate([x, y, 0]) {
		cylinder(h=wall_border_width + top_height_max + half_eps, r=cp_base_radius - half_eps);
	}
}

top_height_max=usb_height + wall_border_width;
base_cutout_height=(base_height_max - base_z_offset);
function get_top_z(height) = (top_height_max + wall_border_width - (height - base_cutout_height));
translate([0, -8, 0]) {
	mirror([0, -1, 0]) {
		difference() {
			general_shape(top_height_max);
			usb(get_top_z(usb_height), height=usb_height);
			ethernet(get_top_z(eth_height), height=eth_height);
			hdmi(get_top_z(hdmi_plug_height), height=hdmi_plug_height);
			power(get_top_z(power_height), height=power_height);
			// Clear space for the corners
			corner_top_space(0, 0);
			corner_top_space(0, 2*wall_border_width + width);
			corner_top_space(2*wall_border_width + length, 0);
			corner_top_space(2*wall_border_width + length, width + 2*wall_border_width);
			// Add some ventilation
			for (x = [16:3.5:55]) {
				translate_cube(
					x, 2*wall_border_width, -half_eps,
					1, width - 2*wall_border_width, wall_border_width + eps
				);
			}
		}
		// add corners
		corner_top(0, 0);
		corner_top(0, 2*wall_border_width + width);
		corner_top(2*wall_border_width + length, 0);
		corner_top(2*wall_border_width + length, width + 2*wall_border_width);
		// add top cyl holders
		top_cyl_holder(cp1_offset_x, cp1_offset_y);
		top_cyl_holder(cp2_offset_x, cp2_offset_y);
	}
}


